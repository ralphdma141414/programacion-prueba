﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoring : MonoBehaviour
{
    public Text texter;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        texter = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Texting();
    }

    void Texting()
    {
        texter.text = "Score: " + player.GetComponent<Player>().score;
    }
}
