﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Rigidbody rigid;
    public float speed, rotation;
    [HideInInspector]
    public int direction, legion, score;
    public GameObject model;

    // Start is called before the first frame update
    void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Directioning();
    }

    void Move()
    {

        float h = Input.GetAxisRaw("Horizontal") * Time.deltaTime;
        float v = Input.GetAxisRaw("Vertical") * Time.deltaTime;

        if (h > 0)
        {
            direction = -1;
        }
        else if (h < 0)
        {
            direction = 1;
        }
        else
        {
            direction = 0;
        }
        if (v > 0)
        {
            legion = 1;
        }
        else if (v < 0)
        {
            legion = -1;
        }
        else
        {
            legion = 0;
        }
    }
    void Directioning()
    {
        if (direction != 0 || legion != 0)
        {

            if (direction == 0)
            {
                rotation = legion - 1;
                rotation = -90 * rotation;
            }
            else if (legion == 0)
            {
                rotation = -direction * 90;
            }
            else
            {
                rotation = 90 - 45 * legion;
                rotation *= -direction;
            }
            model.transform.eulerAngles = new Vector3(0, rotation, 0);
            Vector3 vel = rigid.velocity;
            vel.x = direction * speed * -1;
            vel.z = legion * speed;
            rigid.velocity = vel;
        }
        else
        {
            Vector3 vel = rigid.velocity;
            vel.x = 0;
            vel.z = 0;
            rigid.velocity = vel;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            Destroy(other.gameObject);
            score++;
        }
    }
}
